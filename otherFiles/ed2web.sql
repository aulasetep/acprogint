Use ed2web;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;



--
-- Database: `web2ed`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `carrinho`
--

CREATE TABLE `carrinho` (
  `idCarrinho` bigint(20) NOT NULL,
  `idProduto` bigint(20) NOT NULL,
  `idCliente` varchar(13) NOT NULL,
  `total` bigint(20) NOT NULL,
  `qtde` bigint(20) NOT NULL,
  `produto_idProduto` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `idProduto` bigint(20) NOT NULL,
  `idSecao` bigint(20) NOT NULL,
  `nomeProduto` varchar(50) NOT NULL,
  `valor` bigint(20) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `imagem` varchar(50) NOT NULL,
  `Oferta` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`idProduto`, `idSecao`, `nomeProduto`, `valor`, `descricao`, `imagem`, `Oferta`) VALUES
(1, 1, 'Intel Core i3', 200, 'Processador intel', 'assets/img/f.jpg', 1),
(2, 1, 'Intel Core i5', 150, 'Processador intel', 'assets/img/g.jpg', 0),
(3, 2, 'Monitor LG 22"', 500, 'Monitor LG', '', 1),
(4, 2, 'Monitor Samsung 25"', 700, 'Monitor Samsung', '', 0),
(5, 3, 'Notebook Dell I5', 2000, 'Notebook Dell I5', '', 1),
(6, 3, 'Notebook Alienware I7', 4000, 'Notebook Alienware', '', 0),
(7, 4, 'Microcomputador Dell Gamer I7 GTX1080', 3000, 'Microcomputador Dell Gamer', '', 1),
(8, 4, 'Microcomputador Dell Gamer I7 GTX960', 2800, 'Microcomputador Dell Game', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `secao`
--

CREATE TABLE `secao` (
  `idSecao` bigint(20) NOT NULL,
  `nomeSecao` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `secao`
--

INSERT INTO `secao` (`idSecao`, `nomeSecao`) VALUES
(1, 'Processadores'),
(2, 'Monitor'),
(3, 'Notebook'),
(4, 'Computadores');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `cpf` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `senha` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`cpf`, `email`, `nome`, `senha`) VALUES
('416', 'jean@jean', 'Jean', 'jean'),
('442', 'anderson@anderson', 'Anderson', '123');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_carrinho`
--

CREATE TABLE `usuario_carrinho` (
  `usuario_cpf` bigint(20) NOT NULL,
  `carrinho_idCarrinho` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carrinho`
--
ALTER TABLE `carrinho`
  ADD PRIMARY KEY (`idCarrinho`),
  ADD UNIQUE KEY `idCarrinho` (`idCarrinho`),
  ADD KEY `idProduto` (`idProduto`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `FK_2kdxldr7ktu8wowl38i6yj427` (`produto_idProduto`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`idProduto`),
  ADD UNIQUE KEY `idProduto` (`idProduto`),
  ADD KEY `idSecao` (`idSecao`);

--
-- Indexes for table `secao`
--
ALTER TABLE `secao`
  ADD PRIMARY KEY (`idSecao`),
  ADD KEY `idSecao` (`idSecao`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`cpf`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `cpf` (`cpf`);

--
-- Indexes for table `usuario_carrinho`
--
ALTER TABLE `usuario_carrinho`
  ADD UNIQUE KEY `UK_nyqsi2qr2u9at80krqum9xlfv` (`carrinho_idCarrinho`),
  ADD KEY `FK_am1k6mod161hx8a1sbke43qeu` (`usuario_cpf`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carrinho`
--
ALTER TABLE `carrinho`
  MODIFY `idCarrinho` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `idProduto` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `secao`
--
ALTER TABLE `secao`
  MODIFY `idSecao` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `carrinho`
--
ALTER TABLE `carrinho`
  ADD CONSTRAINT `FK_2kdxldr7ktu8wowl38i6yj427` FOREIGN KEY (`produto_idProduto`) REFERENCES `produto` (`idProduto`),
  ADD CONSTRAINT `carrinhoCliente` FOREIGN KEY (`idCliente`) REFERENCES `usuario` (`cpf`),
  ADD CONSTRAINT `carrinhoProduto` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`);

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `secao` FOREIGN KEY (`idSecao`) REFERENCES `secao` (`idSecao`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
