/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

import entidades.CarrinhoCompra;
import entidades.Cliente;
import entidades.Produto;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class CarrinhoCompraServico {

    private ClienteServico clienteServico = new ClienteServico();
    private Session session;
    private Transaction trans;

    public boolean adicionarProduto(Produto produto) throws IOException {
        session = HibernateUtil.getSessionFactory().openSession();
        trans = session.beginTransaction();

        Cliente cliente = new Cliente();
        CarrinhoCompra carrinho = new CarrinhoCompra();

        try {
            cliente = clienteServico.clienteLogado();
            for (CarrinhoCompra carrinhoCliente : cliente.getCarrinho()) {
                if (carrinhoCliente.getProduto().equals(produto)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Produto já está no Carrinho."));
                    return true;
                }
            }
            carrinho.setProduto(produto);
            carrinho.setQuantidade(1);
            carrinho.setSubTotal(produto.getPreco());
            cliente.getCarrinho().add(carrinho);
            
            session.saveOrUpdate(cliente);
            trans.commit();
            FacesContext.getCurrentInstance().getExternalContext().redirect("./carrinhoCompra.xhtml");
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            session.close();
        }
    }

    public void removerProduto(CarrinhoCompra carrinho) {
        session = HibernateUtil.getSessionFactory().openSession();
        trans = session.beginTransaction();

        Cliente cliente = new Cliente();
        cliente = clienteServico.clienteLogado();
        cliente.getCarrinho().remove(carrinho);
        
        try {
            session.delete(carrinho);
            trans.commit();

        } catch (Exception e) {
        } finally {
            session.close();
        }
    }

    public List<CarrinhoCompra> listarProdutosCarrinho() {
        try {

            Cliente cliente = clienteServico.clienteLogado();
            return cliente.getCarrinho();

        } catch (Exception e) {
            System.out.println("Erro ao Buscar produtos.");
        }
        return null;
    }

    public void atualizarCarrinho(CarrinhoCompra carrinho) {
        session = HibernateUtil.getSessionFactory().openSession();
        trans = session.beginTransaction();
        System.out.println(carrinho.getQuantidade());

        try {

            carrinho.calcularSubTotal();
            session.saveOrUpdate(carrinho);
            trans.commit();

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            session.close();
        }
    }
}
