/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

import entidades.Categoria;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class CategoriaServico {

    private Session session;
    private Transaction trans;

    public List<Categoria> listarCategorias() {
        session = HibernateUtil.getSessionFactory().openSession();
        trans = session.beginTransaction();

        try {
            
            return (List<Categoria>) session.createQuery("from Categoria").list();

        } catch (Exception e) {
            System.out.println("Erro ao Buscar Categorias.");
        } finally {
            session.close();
        }
        return null;
    }

}
