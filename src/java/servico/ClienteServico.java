/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;


import entidades.Cliente;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class ClienteServico {

    private Session session;
    private Transaction trans;

    public boolean salvarCliente(Cliente cliente) {
        
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            trans = session.beginTransaction();          
            session.save(cliente);
            trans.commit();
            return true;
        } catch (Exception e) {
             return false;
            
        } finally {
            session.close();
        }
    }

    public String validarCliente(Cliente cliente) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            trans = session.beginTransaction();
            cliente = (Cliente) session.createQuery(Cliente.LOGAR).setString("login", cliente.getLogin()).setString("senha", cliente.getSenha()).uniqueResult();
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("cliente");
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("cliente", cliente);
            return cliente.getNome();

        } catch (Exception e) {
            System.out.println("Erro ao Loga.");
            System.out.println(e);
        } finally {
            session.close();
        }
        return null;
    }

    public Cliente clienteLogado() {
             
        try {
          
            return (Cliente) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cliente");
             

        } catch (Exception e) {
            
            return null;
        }
    }

    public void deslogarCliente() {
       FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("cliente");
    }
}
