/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

import entidades.Categoria;
import entidades.Cliente;
import entidades.Produto;
import java.util.List;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class ProdutoServico {

    private Session session;
    private Transaction trans;

    public List<Produto> listarProdutosDestaque() {
        session = HibernateUtil.getSessionFactory().openSession();
            trans = session.beginTransaction();
        try {
              return (List<Produto>) session.createQuery(Produto.OBTER_PRODUTOS_EM_DESTAQUE).list();

        } catch (Exception e) {
            System.out.println("Erro ao Pesquisar Produto em Destaque.");
        } finally {
            session.close();
        }
        return null;
    }

    public List<Produto> pesquisarProdutoNome(String nomeProduto) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            trans = session.beginTransaction();

            Cliente cliente = (Cliente) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cliente");
            return (List<Produto>) session.createQuery(Produto.OBTER_PELO_NOME).setString("produto", "%" + nomeProduto + "%").list();

        } catch (Exception e) {
            System.out.println("Erro ao Buscar produtos.");
        } finally {
            session.close();
        }
        return null;
    }

    public List<Produto> filtrarPorCategoria(Categoria categoria) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            trans = session.beginTransaction();

            Cliente cliente = (Cliente) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("cliente");
            return (List<Produto>) session.createQuery(Produto.OBTER_POR_CATEGORIA).setLong("id", categoria.getId()).list();

        } catch (Exception e) {
            System.out.println("Erro ao Buscar produtos.");
        } finally {
            session.close();
        }
        return null;

    }

}
