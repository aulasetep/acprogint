/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entidades.CarrinhoCompra;
import entidades.Cliente;
import entidades.Produto;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import servico.CarrinhoCompraServico;

/**
 *
 * @author santosrts
 */
@ManagedBean
@RequestScoped
public class CarrinhodeCompraBean {

    private CarrinhoCompraServico carrinhoCompraServico = new CarrinhoCompraServico();
    private float valorTotal;

    public void adicionarProduto(Produto produto) throws IOException {

        if (!carrinhoCompraServico.adicionarProduto(produto)) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("./login.xhtml");
        }else{
            somarTotal();
        }
        
         
    }

    public void removerProduto(CarrinhoCompra carrinho) {
        System.out.println("Remover produtos no Carrinho.");
        carrinhoCompraServico.removerProduto(carrinho);
        somarTotal();

    }

    public List<CarrinhoCompra> listarProdutosCarrinho() {
        try {
            somarTotal();
            return carrinhoCompraServico.listarProdutosCarrinho();
        } catch (Exception e) {
            return null;
        }

    }

    public void somarTotal() {
        valorTotal = 0;
        for (CarrinhoCompra carrinhoCliente : carrinhoCompraServico.listarProdutosCarrinho()) {
            valorTotal += carrinhoCliente.getSubTotal();
        }
        
    }

    public void atualizarCarrinho(CarrinhoCompra carrinho, int quantidade) {
        System.out.println(quantidade);
        carrinho.setQuantidade(quantidade);
        carrinhoCompraServico.atualizarCarrinho(carrinho);
        somarTotal();
        
         
    }

    /**
     * @return the valorTotal
     */
    public float getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

}
