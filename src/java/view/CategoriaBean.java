 
package view;

import entidades.Categoria;
import entidades.Produto;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import servico.CategoriaServico;

@ManagedBean
@SessionScoped
public class CategoriaBean {
    
    private CategoriaServico categoriaServico = new CategoriaServico();
     
    public List<Categoria> listarCategoria(){
        return categoriaServico.listarCategorias();
    }

     
     
}
