/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entidades.Cliente;
import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import servico.ClienteServico;

/**
 *
 * @author santosrts
 */
@ManagedBean
@RequestScoped
public class ClienteBean {
    private ClienteServico clienteServico = new ClienteServico();
    private Cliente cliente = new Cliente();
    private String usuarioLogado;
    
            
    public void salvarCliente(){
        
        if (! clienteServico.salvarCliente(getCliente())){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Info", "Usuário Existente."));
        }
          
    }
    
    public void validarCliente() throws IOException {
        usuarioLogado = clienteServico.validarCliente(cliente);
        if (usuarioLogado != null){
        FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.xhtml");
        }else{
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Info", "Usuário ou Senha Incorretos."));
        }
    }
    
    public void deslogarCliente() throws IOException {
          clienteServico.deslogarCliente();
           FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.xhtml");
    }
    
    public String getUsuarioLogado(){
        try{
            usuarioLogado =  clienteServico.clienteLogado().getNome();
            return usuarioLogado;
        }catch (Exception e){
            return null;
        }
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    

     
    public void setUsuarioLogado(String usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    
    
          
     
}
