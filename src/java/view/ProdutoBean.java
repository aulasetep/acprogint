/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import entidades.Categoria;
import entidades.Produto;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import servico.ProdutoServico;


@ManagedBean
@SessionScoped
public class ProdutoBean {

    private ProdutoServico produtoServico = new ProdutoServico();
    private Produto produto = new Produto();
    private String produtoPesquisa = "";
    private Produto produtoSelecionado = new Produto();
    private List<Produto> produtos = new ArrayList<Produto>();

    public List<Produto> listarProdutos() {
        return produtoServico.listarProdutosDestaque();
    }

    public void selecionarProduto(Produto produto) throws IOException {
        this.setProdutoSelecionado(produto);
        FacesContext.getCurrentInstance().getExternalContext().redirect("faces/produto.xhtml");

    }

    public void pesquisarProdutoNome() throws IOException {
        if (!produtoPesquisa.equals("")) {

            setProdutos(produtoServico.pesquisarProdutoNome(produtoPesquisa));
            produtoPesquisa = "";
            if (produtos.size() == 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Nenhum Produto Encontrado."));
            } else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("faces/produtoPesquisa.xhtml");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Info", "Favor Informar um Produto."));
        }
    }

    public void filtrarCategoria(Categoria categoria) throws IOException {
        setProdutos(produtoServico.filtrarPorCategoria(categoria));
        FacesContext.getCurrentInstance().getExternalContext().redirect("faces/produtoCategoria.xhtml");

    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the produtoPesquisa
     */
    public String getProdutoPesquisa() {
        return produtoPesquisa;
    }

    /**
     * @param produtoPesquisa the produtoPesquisa to set
     */
    public void setProdutoPesquisa(String produtoPesquisa) {
        this.produtoPesquisa = produtoPesquisa;
    }

    /**
     * @return the produtoSelecionado
     */
    public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    /**
     * @param produtoSelecionado the produtoSelecionado to set
     */
    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    /**
     * @return the produtos
     */
    public List<Produto> getProdutos() {
        return produtos;
    }

    /**
     * @param produtos the produtos to set
     */
    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

}
